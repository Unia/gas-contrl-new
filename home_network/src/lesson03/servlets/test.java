package lesson03.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/test")
public class test extends HttpServlet{
	public static String data="off";
	public static String date="";
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
	//	gpio_contrl();
		out.println("<html><head><title>가스 컨트롤러 작동페이지</title></head>");
		out.println("<body><h1>가스컨트롤러</h1>");
		out.println("<p>가스 컨트롤러 작동 유무 ="+data+"</p>");
		out.println("<p>가스 컨트롤러 최근 업데이트 시간 ="+date+"</p><p></p>");
		
		out.println("<form action='test1' method='post'>");
		
		out.println("<input type='submit' value='ON'><br>");
		out.println("</form>");
		out.println("<form action='test2' method='post'>");
		out.println("<input type='submit' value='OFF'>");
		out.println("</form>");
		out.println("</body></html>");
	}
/*	private void gpio_contrl() throws IOException, InterruptedException{
		System.out.println("<--Pi4J--> Serial Communication Example ... started.");
		System.out.println(" ... connect using settings: 38400, N, 8, 1.");
		System.out.println(" ... data received on serial port should be displayed below.");
        
        // create an instance of the serial communications class
		final Serial serial = SerialFactory.createInstance();
		// create and register the serial data listener
                
		try {
            // open the default serial port provided on the GPIO header
			serial.open(Serial.DEFAULT_COM_PORT,9600);
            
			// continuous loop to keep the program running until the user terminates the program
		//	for (;;) {
				try {
					serial.writeln("1");
                }catch(IllegalStateException ex){
                    ex.printStackTrace();                    
                }
                // wait 1 second before continuing
                Thread.sleep(1000);
		//	}
		}catch(SerialPortException ex) {
			System.out.println(" ==>> SERIAL SETUP FAILED : " + ex.getMessage());
			return;
		}
	}*/
}
