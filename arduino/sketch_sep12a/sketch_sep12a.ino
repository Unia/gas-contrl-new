int k=13;

void setup() {
  // put your setup code here, to run once:
  pinMode(k,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(k,HIGH);
  delay(10000);
  digitalWrite(k,LOW);
  delay(10000);
}
