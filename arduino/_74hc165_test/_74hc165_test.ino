const int load = 4;
const int clockenablepin = 6;
const int datain = 10;
const int clockin = 5;

void setup() {
  pinMode(load, OUTPUT);
  pinMode(clockenablepin, OUTPUT);
  pinMode(clockin,OUTPUT);
  pinMode(datain, INPUT);
  
  
 
  Serial.begin(9600);
}

void loop() {
    digitalWrite(load,LOW);
  delayMicroseconds(5);
  
  digitalWrite(load,HIGH);
  delayMicroseconds(5);
  digitalWrite(clockin,HIGH);
 digitalWrite(clockenablepin,LOW);
  byte incoming = shiftIn(datain, clockin, LSBFIRST);
  digitalWrite(clockenablepin,HIGH);

  Serial.print("Pin State:");
  Serial.print(incoming, BIN);
  Serial.println();
//  delay(500);
}
