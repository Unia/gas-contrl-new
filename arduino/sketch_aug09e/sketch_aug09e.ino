#include <SoftwareSerial.h>

#define SSID "iptime426"

#define PASS "20772077"

#define DST_IP "222.105.251.139/temp.html"

#define send_com wifi.write(nllf)

SoftwareSerial wifi(2, 3) ;


const char nllf[] = {0x0D, 0x0A} ;


int led = 2 ;


long time_req ;


String req ;


void setup() {

  // put your setup code here, to run once:

  wifi.begin(9600) ;

  wifi.setTimeout(5000) ;

  Serial.begin(9600) ;

  pinMode(led, OUTPUT) ;

  wifi.print("AT") ;

  send_com ;

  delay(100) ;

  wifi.print("AT+RST") ;

  send_com ;

  req = "" ;

  time_req = millis() ;

  while (!wifi.find("ready")) {

    if (wifi.available()) Serial.write(wifi.read()) ;

    if (time_req + 5000 < millis()) {

      Serial.println("fail to reset") ;

      return ;

    }

  }

  Serial.println("reset complete") ;


  wifi.print("AT+CWJAP=\"") ;

  wifi.print(SSID) ;

  wifi.print("\",\"") ;

  wifi.print(PASS) ;

  wifi.print("\"") ;

  send_com ;

  time_req = millis() ;

  Serial.println("waiting for connection") ;

  while (!wifi.find("OK")) {

    if (wifi.available()) Serial.write(wifi.read()) ;

    if (time_req + 5000 < millis()) {

      time_req = millis() ;

      wifi.print("AT+CWJAP=\"") ;

      wifi.print(SSID) ;

      wifi.print("\",\"") ;

      wifi.print(PASS) ;

      wifi.print("\"") ;

      send_com ;

      //      return ;

    }

  }

  Serial.println("connection success") ;

  delay(100) ;


  wifi.print("AT+CIPMUX=1") ;

  send_com ;

  time_req = millis() ;

  while (!wifi.find("OK")) {

    if (wifi.available()) {

      char c = wifi.read() ;

      Serial.write(c) ;

    }

    if (time_req + 1000 < millis()) {

      time_req = millis() ;

      wifi.print("AT+CIPMUX=1") ;

      send_com ;

      //      Serial.println("fail to link") ;

      //      return ;

    }

  }

  Serial.println("multiple connection") ;

  delay(100) ;



  wifi.print("AT+CIPSERVER=1,80") ;

  send_com ;

  time_req = millis() ;

  while (!wifi.find("OK")) {

    if (wifi.available()) {

      char c = wifi.read() ;

      Serial.write(c) ;

    }

    if (time_req + 1000 < millis()) {

      time_req = millis() ;

      wifi.print("AT+CIPSERVER=1,80") ;

      send_com ;

      //      Serial.println("fail to link") ;

      //      return ;

    }

  }

  Serial.println("start") ;

}


void loop() {
  //Serial.println("test_loop_start");
  // put your main code here, to run repeatedly:
/*
 while (!wifi.find("+IPD")) ;
  //Serial.println("test_loop_while_end");
*/
  req = "" ;

  Serial.print("receive  : ") ;

  while (wifi.available()) {

    req += String(char(wifi.read())) ;

  }

  Serial.println(req) ;

  if (req.indexOf("1") != -1) digitalWrite(led, HIGH) ;
  delay(1000);
}
