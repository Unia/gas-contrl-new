int dataPin = 13; //pin 14 of 74HC595
int latchPin = 12; // pin 12 of 74HC595
int clockPin = 11; // pin 11 of 74HC595
int outputEnablePin = 3;
int val[10] ={0b11101011,//=0
              0b00101000,//=1
              0b10110011,//=2
              0b10111010,//=3
              0b01111000,//=4
              0b11011010,//=5
              0b11011011,//=6
              0b11101000,//=7
              0b11111011,//=8
              0b11111000//=9
              };
int gnd[3]={0b00000110,0b00000101,0b00000011};
void setup() {
  pinMode(dataPin, OUTPUT);
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(outputEnablePin, OUTPUT); 
   setBrightness(255);

}

void loop() {
  // put your main code here, to run repeatedly:
   updateShiftRegister(1,0);
 //  delay(5);
   updateShiftRegister(2,1);
 //  delay(5);
   updateShiftRegister(3,2);
 //  delay(5);
}
void updateShiftRegister(int a,int b){
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, gnd[b]);
  shiftOut(dataPin, clockPin, MSBFIRST, val[a]);
  digitalWrite(latchPin, HIGH);
}
void setBrightness(byte brightness){
  analogWrite(outputEnablePin, 255-brightness);
}
