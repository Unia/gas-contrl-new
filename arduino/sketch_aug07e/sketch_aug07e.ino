#include <MsTimer2.h>

int number=120;

void setup() {
  Serial.begin(9600);
  // put your setup code here, to run once:
  MsTimer2::set(60000, flash); // 500ms period
  MsTimer2::start();

}
void flash() {
  number-=1;
}
void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(number);
}
