int k=8;
int s=13;

void setup() {
  // put your setup code here, to run once:
  pinMode(k,INPUT);
  pinMode(s,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(k)==HIGH){
    digitalWrite(s,HIGH);
  }else{
    digitalWrite(s,LOW);
  }
}
