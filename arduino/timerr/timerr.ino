#include <MsTimer2.h>

const int dataPin = 13; //pin 14 of 74HC595
const int latchPin = 12; // pin 12 of 74HC595
const int clockPin = 11; // pin 11 of 74HC595

const int load = 4;
const int clockenablepin = 6;
const int datain = 10;
const int clockin = 5;


int val[10] ={0b11101011,//=0
              0b00101000,//=1
              0b10110011,//=2
              0b10111010,//=3
              0b01111000,//=4
              0b11011010,//=5
              0b11011011,//=6
              0b11101000,//=7
              0b11111011,//=8
              0b11111000//=9
              };
int gnd[3]={0b00000110,0b00000101,0b00000011};
int count=0;
int Time=60;//초기 타이머 60분
byte beck_incoming=0b11111000;
int Time_motion=0;
long time;


void flash() {
  int m=Time/100;
  int t=(Time%100)/10;
  int s=Time%10;
  updateShiftRegister(m,0);
  updateShiftRegister(t,1);
  updateShiftRegister(s,2);
  count+=1;
  if(count==12000){
     Time-=1;
     count=0;
  }
}
void updateShiftRegister(int a,int b){
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, gnd[b]);
  shiftOut(dataPin, clockPin, MSBFIRST, val[a]);
  digitalWrite(latchPin, HIGH);
}

void setup() {
  pinMode(dataPin, OUTPUT);
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(load, OUTPUT);
  pinMode(clockenablepin, OUTPUT);
  pinMode(clockin,OUTPUT);
  pinMode(datain, INPUT);
  Serial.begin(9600);
  MsTimer2::set(5, flash); // 500ms period
  MsTimer2::start();
  
}

void loop() {
  Serial.println(Time);
  Serial.println(count);
  input_ckeck();
  if(Time==0){
    //모터 작동
  }
}

void input_ckeck() {
  digitalWrite(load,LOW);
  delayMicroseconds(5);
  digitalWrite(load,HIGH);
  delayMicroseconds(5);
  digitalWrite(clockin,HIGH);
  digitalWrite(clockenablepin,LOW);
  byte incoming = shiftIn(datain, clockin, LSBFIRST);
  digitalWrite(clockenablepin,HIGH);
  
  if(~incoming&0x40){//sw1=리셋
    Time=60;
  }
  if(~incoming&0x20){//sw2= +
    Time+=1;
  }
  if(~incoming&0x10){//sw3= -
    Time-=1;
  }
  if(incoming&0x4){//motion
    Time_motion = Time;
    time=millis();
  }
  if((millis()-time)<600000){
    Time = Time_motion;
  }
}
