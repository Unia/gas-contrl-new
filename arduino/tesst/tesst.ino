#include <MsTimer2.h>

const int dataPin = 13; // pin 14 of 74HC595
const int latchPin = 12; // pin 12 of 74HC595
const int clockPin = 11; // pin 11 of 74HC595

const int load = 4;
const int clockenablepin = 6;
const int datain = 10;
const int clockin = 5;

const int moter=7;
const int gas_led = A4;
const int gas = A0;
const int power_led = A5; // power led check
const int motion_pin = 8;

int val[10] ={0b11101011,//=0
              0b00101000,//=1
              0b10110011,//=2
              0b10111010,//=3
              0b01111000,//=4
              0b11011010,//=5
              0b11011011,//=6
              0b11101000,//=7
              0b11111011,//=8
              0b11111000//=9
              };
int gnd[3]={0b00000110,0b00000101,0b00000011};
int count=0;
int Time=20;//초기 타이머 20분
byte beck_incoming=0b11111000;
int Time_motion=0;
byte incoming ;
long timE=0;
boolean first_check=false;
void input_ckeck();
void check();
void motion_check();
void moter_A();
void power_check();
boolean gas_sc();

void flash() {
  int m=Time/100;
  int t=(Time%100)/10;
  int s=Time%10;
  updateShiftRegister(m,0);
  updateShiftRegister(t,1);
  updateShiftRegister(s,2);
  count+=1;
  if(count==12000){
     Time-=1;
     count=0;
  }
}
void updateShiftRegister(int a,int b){
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, gnd[b]);
  shiftOut(dataPin, clockPin, MSBFIRST, val[a]);
  digitalWrite(latchPin, HIGH);
}

void setup() {
  pinMode(dataPin, OUTPUT);
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(load, OUTPUT);
  pinMode(clockenablepin, OUTPUT);
  pinMode(clockin,OUTPUT);
  pinMode(datain, INPUT);
  pinMode(moter,OUTPUT);
  pinMode(motion_pin,INPUT);
  input_ckeck();
  Serial.begin(9600);
  MsTimer2::set(5, flash); // 5ms period
  MsTimer2::start();
  
}

void loop() {
  Serial.println(Time);
  Serial.println(count);
  input_ckeck();
  motion_check();
  check();
  power_check();
  if(Time==0|gas_sc()){
    //모터 작동
    moter_A();
  }
}

void input_ckeck() {
  digitalWrite(load,LOW);
  delayMicroseconds(5);
  digitalWrite(load,HIGH);
  delayMicroseconds(5);
  digitalWrite(clockin,HIGH);
  digitalWrite(clockenablepin,LOW);
  incoming = shiftIn(datain, clockin, LSBFIRST);
  digitalWrite(clockenablepin,HIGH);
}
void  check(){
  if(~incoming&0x40){//sw1=리셋
    Time=20;
  }
  if(~incoming&0x20){//sw2= +
    Time+=1;
    delay(100);
  }
  if(~incoming&0x10){//sw3= -
    Time-=1;
    delay(100);
  }
//  Serial.print("motion ");
  Serial.println((byte)incoming);
//  Serial.print("   result ");
//  Serial.print((byte)(incoming&0x04));
//  Serial.println("");
}
void motion_check(){
  Serial.print("check ");
  Serial.print(digitalRead(motion_pin));
  Serial.println();
  if(first_check==false&(digitalRead(motion_pin)==HIGH)){//motion
    Serial.println("motion check");
    Time_motion = Time;
    timE=millis();
    first_check=true;
  }
  if(Time_motion>0&(millis()-timE)<600000){
    Serial.println("motion check00");
    Time = Time_motion;
  }else if(Time_motion>0&(millis()-timE)>600000){
    Serial.println("motion check01");
    Time_motion=0;
    first_check=false;
  }
}
void moter_A(){
  digitalWrite(moter,HIGH);
  delay(10000);
}
void power_check(){ // 9v에서 5v로 떨어질때
  if(analogRead(A3)<=568){
    analogWrite(power_led,1024);
  }else{
    analogWrite(power_led,0);
  }
}
boolean gas_sc(){
  if(analogRead(A0)>1000){
    analogWrite(gas_led,1024);
    return true;
   }else{
    analogWrite(gas_led,0);
    return false;
   }
}
