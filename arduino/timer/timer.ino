unsigned char FND_DATA[]={0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x27,0x7F,0x6F}; //0~9
unsigned char hour=12, mmin=0, sec=0, msec=0, fnd_cnt=0;
unsigned char dp=0, count=0;
//
void setup() {
    DDRD=0xFF;  DDRB=0x0F; 
    TCCR2=4;  
    OCR2=249;    
    TIMSK|=0x80;  // mega8 16000000/64/(1+249)=1000Hz=1ms
    //TCCR2B=4; OCR2B=249; TIMSK2|=0x04; // UNO mega328
    SREG=0x80;
}
//
void loop() {

}
//
ISR(TIMER2_COMP_vect){     // 1ms     ISR(TIMER2_COMPB_vect){ 
    PORTB=0; // FND off
    if(msec>7) dp=0; else dp=1;
    if(++count>=100){ count=0;
        if(++msec>=10){ msec=0;
            if(++sec>=60){ sec=0;
                if(++mmin>=60){ mmin=0;
                    if(++hour>=13){ hour=1; }
                }
            }
        }
    }
    if     (fnd_cnt==0){ PORTD=FND_DATA[mmin/10]; PORTB=1;}   
    else if(fnd_cnt==1){ PORTD=FND_DATA[mmin%10]; PORTB=2; if(dp)PORTD|=0x80;} 
    else if(fnd_cnt==2){ PORTD=FND_DATA[sec/10];  PORTB=4; }  
    else               { PORTD=FND_DATA[sec%10];  PORTB=8; } 
    if(++fnd_cnt>=4) fnd_cnt=0;
}
