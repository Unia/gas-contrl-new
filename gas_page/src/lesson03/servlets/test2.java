package lesson03.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/test2")
public class test2 extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
	//	request.getParameter("name");
		response.setContentType("text/html; charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		out.println("<html><head><title>자동 결과</title></head>");
		out.println("<body>");
		out.println("<p>off 하였습니다</p>");
		out.println("</body></html>");
		test.data="off";
		test.date= new SimpleDateFormat("yyyy-MM-dd  HH:mm").format(new Date());

		response.addHeader("Refresh", "1;url=test");
	}
}
